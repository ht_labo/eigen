/**
 * @file EigenCore.h
 * @author kaya-tect-studio
 * @brief call this header file instead of <Eigen/Core>
 * @version 0.1
 * @date 2022-06-18
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifdef ARDUINO
  // undefine arduino macro
  #ifdef A0
    #define A0_DUMMY A0
  #undef A0
  #endif
  #ifdef A1
    #define A1_DUMMY A1
  #undef A1
  #endif
  #ifdef B0
    #define B0_DUMMY B0
  #undef B0
  #endif
  #ifdef B1
    #define B1_DUMMY B1
  #undef B1
  #endif
#endif

#include <Eigen/Core>

// redefine
#ifdef A0_DUMMY
  #define A0 A0_DUMMY
#undef A0_DUMMY
#endif
#ifdef A1_DUMMY
  #define A1 A1_DUMMY
#undef A1_DUMMY
#endif
#ifdef B0_DUMMY
  #define B0 B0_DUMMY
#undef B0_DUMMY
#endif
#ifdef B1_DUMMY
  #define B1 B1_DUMMY
#undef B1_DUMMY
#endif